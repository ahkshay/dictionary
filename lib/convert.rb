module Convert
  class DigitsToWords

   def valid_number?(number)
      @numbers = number.to_s
      if @numbers.to_s.match(/^\d+$/).nil? or @numbers.length != 10 or @numbers.split('').any? { |i|  ('0'..'1').include?(i) }
        return false
      end
      return true
    end

    def to_words(number)
      if not valid_number?(number)
        return "Invalid Input Data. Phone Number Should Contain Only Numericals other Than 1's and 0's,Should contain exactly 10 digits. No Alphabets are allowed as well."
      end
      dictionary = read_dictionary
      # take the return of all_possible_digits_combination and compare with dictionary to get the list of available words
      final_words = all_possible_digits_combination.flat_map do |array_of_digits|
        words = array_of_digits.map do |array_of_digit|
          characters = array_of_digit.map { |digit| keypad_dict[digit.to_s] }
          word = characters.shift.product(*characters).map(&:join)
          word = word & dictionary[array_of_digit.length]
          end
        words.shift.product(*words).map { |word| word }
      end
      return final_words.uniq
    end

    private
    def keypad_dict
      # retunrs a dict with number as key and value as collection of mapped words
      { "2"=>["a", "b", "c"],
        "3"=>["d", "e", "f"],
        "4"=>["g", "h", "i"],
        "5"=>["j", "k", "l"],
        "6"=>["m", "n", "o"],
        "7"=>["p", "q", "r", "s"],
        "8"=>["t", "u", "v"],
        "9"=>["w", "x", "y", "z"]
      }
    end

    def read_dictionary
      # return a hash based on lenght of words
      dictionary = {}
      File.foreach("lib/dictionary.txt") do |word|
        word = word.chop.to_s.downcase
        dictionary[word.length] ||= []
        dictionary[word.length] << word
      end
    return dictionary
    end


    def all_possible_digits_combination
      # before converting digits into word, it generates all possible combination of digits
      digits = @numbers.split('').map(&:to_i)
      min_words_count, mx_words_count = 3, 10
      words = ((min_words_count - 1)..(mx_words_count - min_words_count - 1)).flat_map do |length|
        words = []
        token_start = 0
        token_end = length
        words << [digits[token_start..token_end], digits[(token_end + 1)..mx_words_count]]
        word = []
        begin
          word << digits[token_start..token_end]
          token_start = token_end + 1
          if (digits[token_start..mx_words_count].to_a.length / min_words_count) <= 1
            token_end = mx_words_count
          else
            token_end += min_words_count
          end
        end while token_start < mx_words_count
        words << word
        words
      end
      words << [digits]
      words.uniq
    end
  end
end


unless ARGV[0].nil?
  def main
    digits = Convert::DigitsToWords.new
    digits.to_words(ARGV[0])
  end
  p main
end
