require 'spec_helper'
RSpec.describe Convert::DigitsToWords, "#towords" do

  let(:digits) { Convert::DigitsToWords.new }

  context 'verify phone numbers' do

    it 'return nil if number contains 0 or 1' do
      expect(digits.valid_number?(6686078254)).to be false
    end

    it 'return nil if number of digits in I/P not equal to ten' do
      expect(digits.valid_number?(98805474)).to be false
    end

    it 'return phone number if does not contains 0 or 1' do
      expect(digits.valid_number?(6686787825)).to be true
    end

  end

  context 'return all possible words or combinations of words from the provided dictionary' do

    it '6686787825 returns a (long) list with at least these word combinations' do
      expect(digits.to_words(6686787825)).to include(["motortruck"], ["motor", "truck"], ["motor", "usual"], ["noun", "struck"], ["not", "opt", "puck"])
    end

    it '2282668687 returns a (long) list with at least these word combinations' do
      expect(digits.to_words(2282668687)).to include(["catamounts"], ["acta", "mounts"], ["act", "amounts"],["act", "amounts"], ["cat", "boo", "tour"])
    end

  end
end
