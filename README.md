# PhoneWorder Challenge
-----------------------------------------------------------------------------------------------------------
### Setup: Follow the below steps to run the code in you local.

  * git clone https://gitlab.com/ahkshay/dictionary.git

  * cd dictionary

  * bundle install

  * rspec spec/digit_to_words_spec.rb
-----------------------------------------------------------------------------------------------------------
### How to Launch the program

  There are two ways as to how the user can launch the program to test their I/P's:

  * First way is user can supply run-time arguments at the command line by following the syntax specified below:

    Syntax: ruby lib/convert.rb <Args Goes Here.....>
    Eg:     ruby lib/convert.rb 2282668687

  * Pls note that program will return nil in case the supplied I/P is of invalid format i.e, number contains either 0's (or) 1's (or) both and also for the scenario where the input number is not equal to exact 10 digits.  

  * Alternate way is to make use of the supplied gem and then pass arguments to the gem

    * Before trying this make sure that the 'convert' gem is being installed.You can install the gem by typing 'gem install convert -v=0.0.3' on the root directory of the project.

    * Once done with installing the gem fire up an irb console and require the convert gem by typing in require 'convert'. Later you can make calls to the convert module using the following command: Convert::DigitsToWords.new.to_words('6686787820')
-----------------------------------------------------------------------------------------------------------
### Code Explanation: Explains the code-flow in a nutshell

  * Generate all possible combination from entered number before converting into words
   Function "all_possible_digits_combination" does the above job and generate output as show below.
   It return possible combination of numbers before creating words, based on min and max limit of words
   For ex. @returns
      * [[[6, 6, 8], [6, 7, 8, 7, 8, 2, 5]],
      * [[6, 6, 8], [6, 7, 8], [7, 8, 2, 5]],
      * [[6, 6, 8, 6], [7, 8, 7, 8, 2, 5]],
      * [[6, 6, 8, 6], [7, 8, 7], [8, 2, 5]],
      * [[6, 6, 8, 6, 7], [8, 7, 8, 2, 5]],
      * [[6, 6, 8, 6, 7, 8], [7, 8, 2, 5]],
      * [[6, 6, 8, 6, 7, 8, 7], [8, 2, 5]],
      * [[6, 6, 8, 6, 7, 8, 7, 8, 2, 5]]]


  * Function "read_dictionary" collects dictionary words and move them into hash against word length to reduce searching

  * Function "to_words" uses the above generated result(From step 1) to search possible words from dictionary(step 2) based on number array.

  * Based on possible combination of numbers from array we map it to actual characters set.
  ---------------------------------------------------------------------------------------------------------
# Benchmark results

  ##### On an 2.5 GHz Intel Core i5 on MacOS Mojave (10.14.3) the results are as follows:

  * real	0m0.120s
  * user	0m0.085s
  * sys	  0m0.030s
  ----------------------------------------------------------------------------------------------------------
