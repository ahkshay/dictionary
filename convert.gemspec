Gem::Specification.new do |s|
  s.name = %q{convert}
  s.version = "0.0.3"
  s.date = Date.today
  s.authors = ["ahkshaey"]
  s.email = ["ahkshaey@gmail.com"]
  s.summary = %q{convert : return all possible words or combinations of words from the provided dictionary,}
  s.files = [
    "lib/convert.rb"
  ]
  s.require_paths = ["lib"]
end
